<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GcLocalLecturasFixture
 */
class GcLocalLecturasFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'lec_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'lec_dispositivo' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'lec_estado' => ['type' => 'char', 'length' => 1, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'lec_temperatura' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lec_amperaje' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lec_fecha_lectura' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'lec_fecha_registro' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['lec_id'], 'length' => []],
            'lec_id_UNIQUE' => ['type' => 'unique', 'columns' => ['lec_id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'lec_id' => 1,
                'lec_dispositivo' => 1,
                'lec_estado' => '',
                'lec_temperatura' => 1.5,
                'lec_amperaje' => 1.5,
                'lec_fecha_lectura' => '2020-09-02 08:24:51',
                'lec_fecha_registro' => '2020-09-02 08:24:51',
            ],
        ];
        parent::init();
    }
}
