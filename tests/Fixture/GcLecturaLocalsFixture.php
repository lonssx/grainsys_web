<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GcLecturaLocalsFixture
 */
class GcLecturaLocalsFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'llc_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'llc_dispositivo' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'llc_estado' => ['type' => 'char', 'length' => 1, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null],
        'llc_monitor_1' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'llc_monitor_2' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'llc_monitor_3' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'llc_monitor_4' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'llc_monitor_5' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'llc_monitor_6' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'llc_fecha_lectura' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'llc_fecha_registro' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => true, 'default' => null, 'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['llc_id'], 'length' => []],
            'llc_id_UNIQUE' => ['type' => 'unique', 'columns' => ['llc_id', 'llc_dispositivo'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'llc_id' => 1,
                'llc_dispositivo' => 1,
                'llc_estado' => '',
                'llc_monitor_1' => 1.5,
                'llc_monitor_2' => 1.5,
                'llc_monitor_3' => 1.5,
                'llc_monitor_4' => 1.5,
                'llc_monitor_5' => 1.5,
                'llc_monitor_6' => 1.5,
                'llc_fecha_lectura' => '2021-02-04 10:04:15',
                'llc_fecha_registro' => '2021-02-04 10:04:15',
            ],
        ];
        parent::init();
    }
}
