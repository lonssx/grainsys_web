<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * GcLecturasFixture
 */
class GcLecturasFixture extends TestFixture
{
    /**
     * Fields
     *
     * @var array
     */
    // phpcs:disable
    public $fields = [
        'lec_id' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'lec_dispositivo' => ['type' => 'integer', 'length' => null, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'lec_temperatura' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lec_amperaje' => ['type' => 'decimal', 'length' => 19, 'precision' => 6, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => ''],
        'lec_fecha_lectura' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        'lec_fecha_registro' => ['type' => 'datetime', 'length' => null, 'precision' => null, 'null' => false, 'default' => null, 'comment' => ''],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['lec_id'], 'length' => []],
            'lec_id_UNIQUE' => ['type' => 'unique', 'columns' => ['lec_id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // phpcs:enable
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'lec_id' => 1,
                'lec_dispositivo' => 1,
                'lec_temperatura' => 1.5,
                'lec_amperaje' => 1.5,
                'lec_fecha_lectura' => '2020-08-20 10:14:41',
                'lec_fecha_registro' => '2020-08-20 10:14:41',
            ],
        ];
        parent::init();
    }
}
