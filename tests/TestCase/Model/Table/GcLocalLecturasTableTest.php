<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GcLocalLecturasTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GcLocalLecturasTable Test Case
 */
class GcLocalLecturasTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GcLocalLecturasTable
     */
    protected $GcLocalLecturas;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.GcLocalLecturas',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('GcLocalLecturas') ? [] : ['className' => GcLocalLecturasTable::class];
        $this->GcLocalLecturas = $this->getTableLocator()->get('GcLocalLecturas', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->GcLocalLecturas);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
