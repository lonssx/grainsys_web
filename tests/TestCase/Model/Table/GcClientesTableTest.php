<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GcClientesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GcClientesTable Test Case
 */
class GcClientesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GcClientesTable
     */
    protected $GcClientes;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.GcClientes',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('GcClientes') ? [] : ['className' => GcClientesTable::class];
        $this->GcClientes = $this->getTableLocator()->get('GcClientes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->GcClientes);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
