<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GcLecturaLocalsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GcLecturaLocalsTable Test Case
 */
class GcLecturaLocalsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\GcLecturaLocalsTable
     */
    protected $GcLecturaLocals;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.GcLecturaLocals',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('GcLecturaLocals') ? [] : ['className' => GcLecturaLocalsTable::class];
        $this->GcLecturaLocals = $this->getTableLocator()->get('GcLecturaLocals', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->GcLecturaLocals);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
