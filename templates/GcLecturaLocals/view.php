<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLecturaLocal $gcLecturaLocal
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Gc Lectura Local'), ['action' => 'edit', $gcLecturaLocal->llc_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Gc Lectura Local'), ['action' => 'delete', $gcLecturaLocal->llc_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcLecturaLocal->llc_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Gc Lectura Locals'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Gc Lectura Local'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLecturaLocals view content">
            <h3><?= h($gcLecturaLocal->llc_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Llc Estado') ?></th>
                    <td><?= h($gcLecturaLocal->llc_estado) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Id') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Dispositivo') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_dispositivo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Lectura 1') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_1) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Lectura 2') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_2) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Lectura 3') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_3) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Lectura 4') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_4) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Lectura 5') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_5) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Lectura 6') ?></th>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_6) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Fecha Lectura') ?></th>
                    <td><?= h($gcLecturaLocal->llc_fecha_lectura) ?></td>
                </tr>
                <tr>
                    <th><?= __('Llc Fecha Registro') ?></th>
                    <td><?= h($gcLecturaLocal->llc_fecha_registro) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
