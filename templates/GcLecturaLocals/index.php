<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLecturaLocal[]|\Cake\Collection\CollectionInterface $gcLecturaLocals
 */
?>
<div class="gcLecturaLocals index content">
    <?= $this->Html->link(__('New Gc Lectura Local'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Gc Lectura Locals') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('llc_id') ?></th>
                    <th><?= $this->Paginator->sort('llc_dispositivo') ?></th>
                    <th><?= $this->Paginator->sort('llc_estado') ?></th>
                    <th><?= $this->Paginator->sort('llc_lectura_1') ?></th>
                    <th><?= $this->Paginator->sort('llc_lectura_2') ?></th>
                    <th><?= $this->Paginator->sort('llc_lectura_3') ?></th>
                    <th><?= $this->Paginator->sort('llc_lectura_4') ?></th>
                    <th><?= $this->Paginator->sort('llc_lectura_5') ?></th>
                    <th><?= $this->Paginator->sort('llc_lectura_6') ?></th>
                    <th><?= $this->Paginator->sort('llc_fecha_lectura') ?></th>
                    <th><?= $this->Paginator->sort('llc_fecha_registro') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($gcLecturaLocals as $gcLecturaLocal): ?>
                <tr>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_id) ?></td>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_dispositivo) ?></td>
                    <td><?= h($gcLecturaLocal->llc_estado) ?></td>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_1) ?></td>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_2) ?></td>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_3) ?></td>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_4) ?></td>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_5) ?></td>
                    <td><?= $this->Number->format($gcLecturaLocal->llc_lectura_6) ?></td>
                    <td><?= h($gcLecturaLocal->llc_fecha_lectura) ?></td>
                    <td><?= h($gcLecturaLocal->llc_fecha_registro) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $gcLecturaLocal->llc_id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gcLecturaLocal->llc_id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gcLecturaLocal->llc_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcLecturaLocal->llc_id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
