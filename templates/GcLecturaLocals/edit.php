<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLecturaLocal $gcLecturaLocal
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $gcLecturaLocal->llc_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $gcLecturaLocal->llc_id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Gc Lectura Locals'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLecturaLocals form content">
            <?= $this->Form->create($gcLecturaLocal) ?>
            <fieldset>
                <legend><?= __('Edit Gc Lectura Local') ?></legend>
                <?php
                    echo $this->Form->control('llc_dispositivo');
                    echo $this->Form->control('llc_estado');
                    echo $this->Form->control('llc_lectura_1');
                    echo $this->Form->control('llc_lectura_2');
                    echo $this->Form->control('llc_lectura_3');
                    echo $this->Form->control('llc_lectura_4');
                    echo $this->Form->control('llc_lectura_5');
                    echo $this->Form->control('llc_lectura_6');
                    echo $this->Form->control('llc_fecha_lectura');
                    echo $this->Form->control('llc_fecha_registro', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
