<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcDispositivo $gcDispositivo
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Gc Dispositivo'), ['action' => 'edit', $gcDispositivo->dip_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Gc Dispositivo'), ['action' => 'delete', $gcDispositivo->dip_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcDispositivo->dip_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Gc Dispositivos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Gc Dispositivo'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcDispositivos view content">
            <h3><?= h($gcDispositivo->dip_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Dip Id Configuracion') ?></th>
                    <td><?= $this->Number->format($gcDispositivo->dip_id_configuracion) ?></td>
                </tr>            
                <tr>
                    <th><?= __('Dip Codigo') ?></th>
                    <td><?= h($gcDispositivo->dip_codigo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Ip') ?></th>
                    <td><?= h($gcDispositivo->dip_ip) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Mac') ?></th>
                    <td><?= h($gcDispositivo->dip_mac) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Estado') ?></th>
                    <td><?= h($gcDispositivo->dip_estado) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Id') ?></th>
                    <td><?= $this->Number->format($gcDispositivo->dip_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Cliente') ?></th>
                    <td><?= $this->Number->format($gcDispositivo->dip_cliente) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Tipo') ?></th>
                    <td><?= $this->Number->format($gcDispositivo->dip_tipo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Fecha Registro') ?></th>
                    <td><?= h($gcDispositivo->dip_fecha_registro) ?></td>
                </tr>
                <tr>
                    <th><?= __('Dip Fecha Activacion') ?></th>
                    <td><?= h($gcDispositivo->dip_fecha_activacion) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
