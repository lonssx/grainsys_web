<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcDispositivo $gcDispositivo
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Gc Dispositivos'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcDispositivos form content">
            <?= $this->Form->create($gcDispositivo) ?>
            <fieldset>
                <legend><?= __('Add Gc Dispositivo') ?></legend>
                <?php
                    echo $this->Form->control('dip_cliente');
                    echo $this->Form->control('dip_id_configuracion');
                    echo $this->Form->control('dip_codigo');
                    echo $this->Form->control('dip_tipo');
                    echo $this->Form->control('dip_ip');
                    echo $this->Form->control('dip_mac');
                    echo $this->Form->control('dip_estado');
                    echo $this->Form->control('dip_fecha_registro', ['empty' => true]);
                    echo $this->Form->control('dip_fecha_activacion', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
