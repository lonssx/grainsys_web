<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcDispositivo[]|\Cake\Collection\CollectionInterface $gcDispositivos
 */
?>
<div class="gcDispositivos index content">
    <?= $this->Html->link(__('New Gc Dispositivo'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Gc Dispositivos') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('dip_id') ?></th>
                    <th><?= $this->Paginator->sort('dip_cliente') ?></th>
                    <th><?= $this->Paginator->sort('dip_id_configuracion') ?></th>
                    <th><?= $this->Paginator->sort('dip_codigo') ?></th>
                    <th><?= $this->Paginator->sort('dip_tipo') ?></th>
                    <th><?= $this->Paginator->sort('dip_ip') ?></th>
                    <th><?= $this->Paginator->sort('dip_mac') ?></th>
                    <th><?= $this->Paginator->sort('dip_estado') ?></th>
                    <th><?= $this->Paginator->sort('dip_fecha_registro') ?></th>
                    <th><?= $this->Paginator->sort('dip_fecha_activacion') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($gcDispositivos as $gcDispositivo): ?>
                <tr>
                    <td><?= $this->Number->format($gcDispositivo->dip_id) ?></td>
                    <td><?= $this->Number->format($gcDispositivo->dip_cliente) ?></td>
                    <td><?= h($gcDispositivo->dip_codigo) ?></td>
                    <td><?= $this->Number->format($gcDispositivo->dip_tipo) ?></td>
                    <td><?= h($gcDispositivo->dip_ip) ?></td>
                    <td><?= h($gcDispositivo->dip_mac) ?></td>
                    <td><?= h($gcDispositivo->dip_estado) ?></td>
                    <td><?= h($gcDispositivo->dip_fecha_registro) ?></td>
                    <td><?= h($gcDispositivo->dip_fecha_activacion) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $gcDispositivo->dip_id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gcDispositivo->dip_id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gcDispositivo->dip_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcDispositivo->dip_id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
