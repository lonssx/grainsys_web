<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLocalLectura $gcLocalLectura
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Gc Local Lectura'), ['action' => 'edit', $gcLocalLectura->lec_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Gc Local Lectura'), ['action' => 'delete', $gcLocalLectura->lec_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcLocalLectura->lec_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Gc Local Lecturas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Gc Local Lectura'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLocalLecturas view content">
            <h3><?= h($gcLocalLectura->lec_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Lec Estado') ?></th>
                    <td><?= h($gcLocalLectura->lec_estado) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Id') ?></th>
                    <td><?= $this->Number->format($gcLocalLectura->lec_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Dispositivo') ?></th>
                    <td><?= $this->Number->format($gcLocalLectura->lec_dispositivo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Temperatura') ?></th>
                    <td><?= $this->Number->format($gcLocalLectura->lec_temperatura) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Amperaje') ?></th>
                    <td><?= $this->Number->format($gcLocalLectura->lec_amperaje) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Fecha Lectura') ?></th>
                    <td><?= h($gcLocalLectura->lec_fecha_lectura) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Fecha Registro') ?></th>
                    <td><?= h($gcLocalLectura->lec_fecha_registro) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
