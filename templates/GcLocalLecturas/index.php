<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLocalLectura[]|\Cake\Collection\CollectionInterface $gcLocalLecturas
 */
?>
<div class="gcLocalLecturas index content">
    <?= $this->Html->link(__('New Gc Local Lectura'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Gc Local Lecturas') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('lec_id') ?></th>
                    <th><?= $this->Paginator->sort('lec_dispositivo') ?></th>
                    <th><?= $this->Paginator->sort('lec_estado') ?></th>
                    <th><?= $this->Paginator->sort('lec_temperatura') ?></th>
                    <th><?= $this->Paginator->sort('lec_amperaje') ?></th>
                    <th><?= $this->Paginator->sort('lec_fecha_lectura') ?></th>
                    <th><?= $this->Paginator->sort('lec_fecha_registro') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($gcLocalLecturas as $gcLocalLectura): ?>
                <tr>
                    <td><?= $this->Number->format($gcLocalLectura->lec_id) ?></td>
                    <td><?= $this->Number->format($gcLocalLectura->lec_dispositivo) ?></td>
                    <td><?= h($gcLocalLectura->lec_estado) ?></td>
                    <td><?= $this->Number->format($gcLocalLectura->lec_temperatura) ?></td>
                    <td><?= $this->Number->format($gcLocalLectura->lec_amperaje) ?></td>
                    <td><?= h($gcLocalLectura->lec_fecha_lectura) ?></td>
                    <td><?= h($gcLocalLectura->lec_fecha_registro) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $gcLocalLectura->lec_id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gcLocalLectura->lec_id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gcLocalLectura->lec_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcLocalLectura->lec_id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
