<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLocalLectura $gcLocalLectura
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Gc Local Lecturas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLocalLecturas form content">
            <?= $this->Form->create($gcLocalLectura) ?>
            <fieldset>
                <legend><?= __('Add Gc Local Lectura') ?></legend>
                <?php
                    echo $this->Form->control('lec_dispositivo');
                    echo $this->Form->control('lec_estado');
                    echo $this->Form->control('lec_temperatura');
                    echo $this->Form->control('lec_amperaje');
                    echo $this->Form->control('lec_fecha_lectura');
                    echo $this->Form->control('lec_fecha_registro', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
