<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLocalLectura $gcLocalLectura
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $gcLocalLectura->lec_id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $gcLocalLectura->lec_id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Gc Local Lecturas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLocalLecturas form content">
            <?= $this->Form->create($gcLocalLectura) ?>
            <fieldset>
                <legend><?= __('Edit Gc Local Lectura') ?></legend>
                <?php
                    echo $this->Form->control('lec_dispositivo');
                    echo $this->Form->control('lec_estado');
                    echo $this->Form->control('lec_temperatura');
                    echo $this->Form->control('lec_amperaje');
                    echo $this->Form->control('lec_fecha_lectura');
                    echo $this->Form->control('lec_fecha_registro', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
