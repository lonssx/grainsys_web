<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcCliente $gcCliente
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Gc Clientes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcClientes form content">
            <?= $this->Form->create($gcCliente) ?>
            <fieldset>
                <legend><?= __('Add Gc Cliente') ?></legend>
                <?php
                    echo $this->Form->control('cli_nombre');
                    echo $this->Form->control('cli_email');
                    echo $this->Form->control('cli_telefono');
                    echo $this->Form->control('cli_contacto');
                    echo $this->Form->control('cli_pais');
                    echo $this->Form->control('cli_provincia');
                    echo $this->Form->control('cli_estado');
                    echo $this->Form->control('cli_fecha_registro');
                    echo $this->Form->control('cli_fecha_modificacion', ['empty' => true]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
