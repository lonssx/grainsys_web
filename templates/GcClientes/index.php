<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcCliente[]|\Cake\Collection\CollectionInterface $gcClientes
 */
?>
<div class="gcClientes index content">
    <?= $this->Html->link(__('New Gc Cliente'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Gc Clientes') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('cli_id') ?></th>
                    <th><?= $this->Paginator->sort('cli_nombre') ?></th>
                    <th><?= $this->Paginator->sort('cli_email') ?></th>
                    <th><?= $this->Paginator->sort('cli_telefono') ?></th>
                    <th><?= $this->Paginator->sort('cli_contacto') ?></th>
                    <th><?= $this->Paginator->sort('cli_pais') ?></th>
                    <th><?= $this->Paginator->sort('cli_provincia') ?></th>
                    <th><?= $this->Paginator->sort('cli_estado') ?></th>
                    <th><?= $this->Paginator->sort('cli_fecha_registro') ?></th>
                    <th><?= $this->Paginator->sort('cli_fecha_modificacion') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($gcClientes as $gcCliente): ?>
                <tr>
                    <td><?= $this->Number->format($gcCliente->cli_id) ?></td>
                    <td><?= h($gcCliente->cli_nombre) ?></td>
                    <td><?= h($gcCliente->cli_email) ?></td>
                    <td><?= h($gcCliente->cli_telefono) ?></td>
                    <td><?= h($gcCliente->cli_contacto) ?></td>
                    <td><?= $this->Number->format($gcCliente->cli_pais) ?></td>
                    <td><?= $this->Number->format($gcCliente->cli_provincia) ?></td>
                    <td><?= h($gcCliente->cli_estado) ?></td>
                    <td><?= h($gcCliente->cli_fecha_registro) ?></td>
                    <td><?= h($gcCliente->cli_fecha_modificacion) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $gcCliente->cli_id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gcCliente->cli_id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gcCliente->cli_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcCliente->cli_id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
