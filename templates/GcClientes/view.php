<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcCliente $gcCliente
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Gc Cliente'), ['action' => 'edit', $gcCliente->cli_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Gc Cliente'), ['action' => 'delete', $gcCliente->cli_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcCliente->cli_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Gc Clientes'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Gc Cliente'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcClientes view content">
            <h3><?= h($gcCliente->cli_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Cli Nombre') ?></th>
                    <td><?= h($gcCliente->cli_nombre) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Email') ?></th>
                    <td><?= h($gcCliente->cli_email) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Telefono') ?></th>
                    <td><?= h($gcCliente->cli_telefono) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Contacto') ?></th>
                    <td><?= h($gcCliente->cli_contacto) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Estado') ?></th>
                    <td><?= h($gcCliente->cli_estado) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Id') ?></th>
                    <td><?= $this->Number->format($gcCliente->cli_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Pais') ?></th>
                    <td><?= $this->Number->format($gcCliente->cli_pais) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Provincia') ?></th>
                    <td><?= $this->Number->format($gcCliente->cli_provincia) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Fecha Registro') ?></th>
                    <td><?= h($gcCliente->cli_fecha_registro) ?></td>
                </tr>
                <tr>
                    <th><?= __('Cli Fecha Modificacion') ?></th>
                    <td><?= h($gcCliente->cli_fecha_modificacion) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
