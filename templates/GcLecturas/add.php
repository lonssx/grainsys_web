<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLectura $gcLectura
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Gc Lecturas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLecturas form content">
            <?= $this->Form->create($gcLectura) ?>
            <fieldset>
                <legend><?= __('Add Gc Lectura') ?></legend>
                <?php
                    echo $this->Form->control('lec_dispositivo');
                    echo $this->Form->control('lec_id_remoto');
                    echo $this->Form->control('lec_estado');
                    echo $this->Form->control('lec_lectura_1');
                    echo $this->Form->control('lec_lectura_2');
                    echo $this->Form->control('lec_lectura_3');
                    echo $this->Form->control('lec_lectura_4');
                    echo $this->Form->control('lec_lectura_5');
                    echo $this->Form->control('lec_lectura_6');
                    echo $this->Form->control('lec_fecha_lectura');
                    //echo $this->Form->control('lec_fecha_registro');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
