<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLectura $gcLectura
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Gc Lectura'), ['action' => 'edit', $gcLectura->lec_id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Gc Lectura'), ['action' => 'delete', $gcLectura->lec_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcLectura->lec_id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Gc Lecturas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Gc Lectura'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLecturas view content">
            <h3><?= h($gcLectura->lec_id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Lec Id') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Dispositivo') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_dispositivo) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Lectura 1') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_lectura_1) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Lectura 2') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_lectura_2) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Lectura 3') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_lectura_3) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Lectura 4') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_lectura_4) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Lectura 5') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_lectura_5) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Lectura 6') ?></th>
                    <td><?= $this->Number->format($gcLectura->lec_lectura_6) ?></td>
                </tr>                                                                
                <tr>
                    <th><?= __('Lec Fecha Lectura') ?></th>
                    <td><?= h($gcLectura->lec_fecha_lectura) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lec Fecha Registro') ?></th>
                    <td><?= h($gcLectura->lec_fecha_registro) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
