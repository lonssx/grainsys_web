<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLectura[]|\Cake\Collection\CollectionInterface $gcLecturas
 */
?>
<div class="gcLecturas index content">
    <?= $this->Html->link(__('New Gc Lectura'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Gc Lecturas') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('lec_id') ?></th>
                    <th><?= $this->Paginator->sort('lec_dispositivo') ?></th>
                    <th><?= $this->Paginator->sort('lec_id_remoto') ?></th>
                    <th><?= $this->Paginator->sort('lec_estado') ?></th>
                    <th><?= $this->Paginator->sort('lec_lectura_1') ?></th>
                    <th><?= $this->Paginator->sort('lec_lectura_2') ?></th>
                    <th><?= $this->Paginator->sort('lec_lectura_3') ?></th>
                    <th><?= $this->Paginator->sort('lec_lectura_4') ?></th>
                    <th><?= $this->Paginator->sort('lec_lectura_5') ?></th>
                    <th><?= $this->Paginator->sort('lec_lectura_6') ?></th>
                    <th><?= $this->Paginator->sort('lec_fecha_lectura') ?></th>
                    <th><?= $this->Paginator->sort('lec_fecha_registro') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($gcLecturas as $gcLectura): ?>
                <tr>
                    <td><?= $this->Number->format($gcLectura->lec_id) ?></td>
                    <td><?= $this->Number->format($gcLectura->lec_dispositivo) ?></td>
                    <td><?= $this->Number->format($gcLectura->lec_id_remoto) ?></td>
                    <td><?= $this->Number->format($gcLectura->lec_estado) ?></td>
                    <td><?= $this->Number->format($gcLectura->lec_temperatura) ?></td>
                    <td><?= $this->Number->format($gcLectura->lec_amperaje) ?></td>
                    <td><?= h($gcLectura->lec_fecha_lectura) ?></td>
                    <td><?= h($gcLectura->lec_fecha_registro) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $gcLectura->lec_id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $gcLectura->lec_id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $gcLectura->lec_id], ['confirm' => __('Are you sure you want to delete # {0}?', $gcLectura->lec_id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
