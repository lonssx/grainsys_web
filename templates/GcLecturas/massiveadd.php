<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\GcLectura $gcLectura
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Gc Lecturas'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="gcLecturas form content">
            <?= $this->Form->create($gcLectura) ?>
            <fieldset>
                <legend><?= __('Add Gc Lectura') ?></legend>
                <?php
                    echo $this->Form->control('id_arduidno_box');
                    echo $this->Form->control('lectura1');
                    echo $this->Form->control('lectura2');
                    echo $this->Form->control('lectura3');
                    echo $this->Form->control('lectura4');
                    echo $this->Form->control('lectura5');
                    echo $this->Form->control('lectura6');
                    echo $this->Form->control('fecha');
                    //echo $this->Form->control('lec_fecha_registro');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
