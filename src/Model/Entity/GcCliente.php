<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GcCliente Entity
 *
 * @property int $cli_id
 * @property string $cli_nombre
 * @property string|null $cli_email
 * @property string|null $cli_telefono
 * @property string|null $cli_contacto
 * @property int $cli_pais
 * @property int $cli_provincia
 * @property string $cli_estado
 * @property \Cake\I18n\FrozenTime $cli_fecha_registro
 * @property \Cake\I18n\FrozenTime|null $cli_fecha_modificacion
 */
class GcCliente extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'cli_nombre' => true,
        'cli_email' => true,
        'cli_telefono' => true,
        'cli_contacto' => true,
        'cli_pais' => true,
        'cli_provincia' => true,
        'cli_estado' => true,
        'cli_fecha_registro' => true,
        'cli_fecha_modificacion' => true,
    ];
}
