<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GcLocalLectura Entity
 *
 * @property int $lec_id
 * @property int $lec_dispositivo
 * @property string|null $lec_estado
 * @property string|null $lec_temperatura
 * @property string|null $lec_amperaje
 * @property \Cake\I18n\FrozenTime $lec_fecha_lectura
 * @property \Cake\I18n\FrozenTime|null $lec_fecha_registro
 */
class GcLocalLectura extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'lec_dispositivo' => true,
        'lec_estado' => true,
        'lec_temperatura' => true,
        'lec_amperaje' => true,
        'lec_fecha_lectura' => true,
        'lec_fecha_registro' => true,
    ];
}
