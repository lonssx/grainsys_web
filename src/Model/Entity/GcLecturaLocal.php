<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GcLecturaLocal Entity
 *
 * @property int $llc_id
 * @property int $llc_dispositivo
 * @property string|null $llc_estado
 * @property string|null $llc_lectura_1
 * @property string|null $llc_lectura_2
 * @property string|null $llc_lectura_3
 * @property string|null $llc_lectura_4
 * @property string|null $llc_lectura_5
 * @property string|null $llc_lectura_6
 * @property \Cake\I18n\FrozenTime $llc_fecha_lectura
 * @property \Cake\I18n\FrozenTime|null $llc_fecha_registro
 */
class GcLecturaLocal extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'llc_dispositivo' => true,
        'llc_estado' => true,
        'llc_lectura_1' => true,
        'llc_lectura_2' => true,
        'llc_lectura_3' => true,
        'llc_lectura_4' => true,
        'llc_lectura_5' => true,
        'llc_lectura_6' => true,
        'llc_fecha_lectura' => true,
        'llc_fecha_registro' => true,
    ];
}
