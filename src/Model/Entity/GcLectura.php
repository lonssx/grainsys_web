<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GcLectura Entity
 *
 * @property int $lec_id
 * @property int $lec_dispositivo
 * @property string|null $lec_temperatura
 * @property string|null $lec_amperaje
 * @property \Cake\I18n\FrozenTime $lec_fecha_lectura
 * @property \Cake\I18n\FrozenTime $lec_fecha_registro
 */
class GcLectura extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'lec_dispositivo' => true,
        'lec_id_remoto'=>true,
        'lec_estado'=>true,
        'lec_lectura_1' => true,
        'lec_lectura_2' => true,
        'lec_lectura_3' => true,
        'lec_lectura_4' => true,
        'lec_lectura_5' => true,
        'lec_lectura_6' => true,
        'lec_fecha_lectura' => true,
        'lec_fecha_registro' => true,
    ];
}
