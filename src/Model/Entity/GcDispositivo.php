<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * GcDispositivo Entity
 *
 * @property int $dip_id
 * @property int $dip_cliente
 * @property string $dip_codigo
 * @property int $dip_tipo
 * @property string|null $dip_ip
 * @property string|null $dip_mac
 * @property string $dip_estado
 * @property \Cake\I18n\FrozenTime|null $dip_fecha_registro
 * @property \Cake\I18n\FrozenTime|null $dip_fecha_activacion
 *
 * @property \App\Model\Entity\Dip $dip
 */
class GcDispositivo extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'dip_cliente' => true,
        'dip_id_configuracion'=>true,
        'dip_codigo' => true,
        'dip_tipo' => true,
        'dip_ip' => true,
        'dip_mac' => true,
        'dip_estado' => true,
        'dip_fecha_registro' => true,
        'dip_fecha_activacion' => true,
        'dip' => true,
    ];
}
