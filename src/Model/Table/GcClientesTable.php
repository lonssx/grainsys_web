<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GcClientes Model
 *
 * @method \App\Model\Entity\GcCliente newEmptyEntity()
 * @method \App\Model\Entity\GcCliente newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\GcCliente[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GcCliente get($primaryKey, $options = [])
 * @method \App\Model\Entity\GcCliente findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\GcCliente patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GcCliente[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\GcCliente|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcCliente saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcCliente[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcCliente[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcCliente[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcCliente[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class GcClientesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gc_clientes');
        $this->setDisplayField('cli_id');
        $this->setPrimaryKey('cli_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('cli_id')
            ->allowEmptyString('cli_id', null, 'create')
            ->add('cli_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('cli_nombre')
            ->maxLength('cli_nombre', 255)
            ->requirePresence('cli_nombre', 'create')
            ->notEmptyString('cli_nombre');

        $validator
            ->scalar('cli_email')
            ->maxLength('cli_email', 100)
            ->allowEmptyString('cli_email');

        $validator
            ->scalar('cli_telefono')
            ->maxLength('cli_telefono', 45)
            ->allowEmptyString('cli_telefono');

        $validator
            ->scalar('cli_contacto')
            ->maxLength('cli_contacto', 45)
            ->allowEmptyString('cli_contacto');

        $validator
            ->integer('cli_pais')
            ->requirePresence('cli_pais', 'create')
            ->notEmptyString('cli_pais');

        $validator
            ->integer('cli_provincia')
            ->requirePresence('cli_provincia', 'create')
            ->notEmptyString('cli_provincia');

        $validator
            ->scalar('cli_estado')
            ->maxLength('cli_estado', 45)
            ->requirePresence('cli_estado', 'create')
            ->notEmptyString('cli_estado');

        $validator
            ->dateTime('cli_fecha_registro')
            ->requirePresence('cli_fecha_registro', 'create')
            ->notEmptyDateTime('cli_fecha_registro');

        $validator
            ->dateTime('cli_fecha_modificacion')
            ->allowEmptyDateTime('cli_fecha_modificacion');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['cli_id']), ['errorField' => 'cli_id']);

        return $rules;
    }
}
