<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GcLecturaLocals Model
 *
 * @method \App\Model\Entity\GcLecturaLocal newEmptyEntity()
 * @method \App\Model\Entity\GcLecturaLocal newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\GcLecturaLocal[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GcLecturaLocal get($primaryKey, $options = [])
 * @method \App\Model\Entity\GcLecturaLocal findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\GcLecturaLocal patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GcLecturaLocal[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\GcLecturaLocal|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcLecturaLocal saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcLecturaLocal[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLecturaLocal[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLecturaLocal[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLecturaLocal[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class GcLecturaLocalsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gc_lectura_locals');
        $this->setDisplayField('llc_id');
        $this->setPrimaryKey('llc_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('llc_id')
            ->allowEmptyString('llc_id', null, 'create')
			->add('llc_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('llc_dispositivo')
            ->requirePresence('llc_dispositivo', 'create')
            ->notEmptyString('llc_dispositivo');

        $validator
            ->scalar('llc_estado')
            ->maxLength('llc_estado', 1)
            ->allowEmptyString('llc_estado');

        $validator
            ->decimal('llc_lectura_1')
            ->allowEmptyString('llc_lectura_1');

        $validator
            ->decimal('llc_lectura_2')
            ->allowEmptyString('llc_lectura_2');

        $validator
            ->decimal('llc_lectura_3')
            ->allowEmptyString('llc_lectura_3');			

        $validator
            ->decimal('llc_lectura_4')
            ->allowEmptyString('llc_lectura_4');

        $validator
            ->decimal('llc_lectura_5')
            ->allowEmptyString('llc_lectura_5');

        $validator
            ->decimal('llc_lectura_6')
            ->allowEmptyString('llc_lectura_6');

        $validator
            ->dateTime('llc_fecha_lectura')
            ->requirePresence('llc_fecha_lectura', 'create')
            ->notEmptyDateTime('llc_fecha_lectura');

        $validator
            ->dateTime('llc_fecha_registro')
            ->allowEmptyDateTime('llc_fecha_registro');

        return $validator;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['lec_id']), ['errorField' => 'lec_id']);

        return $rules;
    }
}
