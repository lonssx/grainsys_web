<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GcLecturas Model
 *
 * @method \App\Model\Entity\GcLectura newEmptyEntity()
 * @method \App\Model\Entity\GcLectura newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\GcLectura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GcLectura get($primaryKey, $options = [])
 * @method \App\Model\Entity\GcLectura findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\GcLectura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GcLectura[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\GcLectura|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcLectura saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcLectura[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLectura[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLectura[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLectura[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class GcLecturasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gc_lecturas');
        $this->setDisplayField('lec_id');
        $this->setPrimaryKey('lec_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('lec_id')
            ->allowEmptyString('lec_id', null, 'create')
            ->add('lec_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('lec_dispositivo')
            ->requirePresence('lec_dispositivo', 'create')
            ->notEmptyString('lec_dispositivo');

        $validator
            ->integer('lec_id_remoto')
            ->requirePresence('lec_id_remoto', 'create')
            ->notEmptyString('lec_id_remoto');            

        $validator
            ->decimal('lec_lectura_1')
            ->allowEmptyString('lec_lectura_1');

        $validator
            ->decimal('lec_lectura_2')
            ->allowEmptyString('lec_lectura_2');

        $validator
            ->decimal('lec_lectura_3')
            ->allowEmptyString('lec_lectura_3');

        $validator
            ->decimal('lec_lectura_4')
            ->allowEmptyString('lec_lectura_4');

        $validator
            ->decimal('lec_lectura_5')
            ->allowEmptyString('lec_lectura_5');

        $validator
            ->decimal('lec_lectura_6')
            ->allowEmptyString('lec_lectura_6');            

        $validator
            ->dateTime('lec_fecha_lectura')
            ->requirePresence('lec_fecha_lectura', 'create')
            ->notEmptyDateTime('lec_fecha_lectura');

        /*$validator
            ->dateTime('lec_fecha_registro');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['lec_id']), ['errorField' => 'lec_id']);

        return $rules;
    }
}
