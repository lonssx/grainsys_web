<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GcLocalLecturas Model
 *
 * @method \App\Model\Entity\GcLocalLectura newEmptyEntity()
 * @method \App\Model\Entity\GcLocalLectura newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\GcLocalLectura[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GcLocalLectura get($primaryKey, $options = [])
 * @method \App\Model\Entity\GcLocalLectura findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\GcLocalLectura patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GcLocalLectura[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\GcLocalLectura|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcLocalLectura saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcLocalLectura[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLocalLectura[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLocalLectura[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcLocalLectura[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class GcLocalLecturasTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gc_local_lecturas');
        $this->setDisplayField('lec_id');
        $this->setPrimaryKey('lec_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('lec_id')
            ->allowEmptyString('lec_id', null, 'create')
            ->add('lec_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('lec_dispositivo')
            ->requirePresence('lec_dispositivo', 'create')
            ->notEmptyString('lec_dispositivo');

        $validator
            ->scalar('lec_estado')
            ->maxLength('lec_estado', 1)
            ->allowEmptyString('lec_estado');

        $validator
            ->decimal('lec_temperatura')
            ->allowEmptyString('lec_temperatura');

        $validator
            ->decimal('lec_amperaje')
            ->allowEmptyString('lec_amperaje');

        $validator
            ->dateTime('lec_fecha_lectura')
            ->requirePresence('lec_fecha_lectura', 'create')
            ->notEmptyDateTime('lec_fecha_lectura');

        $validator
            ->dateTime('lec_fecha_registro')
            ->allowEmptyDateTime('lec_fecha_registro');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['lec_id']), ['errorField' => 'lec_id']);

        return $rules;
    }
}
