<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * GcDispositivos Model
 *
 * @method \App\Model\Entity\GcDispositivo newEmptyEntity()
 * @method \App\Model\Entity\GcDispositivo newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\GcDispositivo[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\GcDispositivo get($primaryKey, $options = [])
 * @method \App\Model\Entity\GcDispositivo findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\GcDispositivo patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\GcDispositivo[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\GcDispositivo|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcDispositivo saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\GcDispositivo[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcDispositivo[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcDispositivo[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\GcDispositivo[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class GcDispositivosTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('gc_dispositivos');
        $this->setDisplayField('dip_id');
        $this->setPrimaryKey('dip_id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('dip_id')
            ->allowEmptyString('dip_id', null, 'create')
            ->add('dip_id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('dip_cliente')
            ->requirePresence('dip_cliente', 'create')
            ->notEmptyString('dip_cliente');

        $validator
            ->integer('dip_id_configuracion')
            ->requirePresence('dip_id_configuracion', 'create')
            ->notEmptyString('dip_id_configuracion');            

        $validator
            ->scalar('dip_codigo')
            ->maxLength('dip_codigo', 100)
            ->requirePresence('dip_codigo', 'create')
            ->notEmptyString('dip_codigo');

        $validator
            ->integer('dip_tipo')
            ->requirePresence('dip_tipo', 'create')
            ->notEmptyString('dip_tipo');

        $validator
            ->scalar('dip_ip')
            ->maxLength('dip_ip', 30)
            ->allowEmptyString('dip_ip');

        $validator
            ->scalar('dip_mac')
            ->maxLength('dip_mac', 45)
            ->allowEmptyString('dip_mac');

        $validator
            ->scalar('dip_estado')
            ->maxLength('dip_estado', 1)
            ->requirePresence('dip_estado', 'create')
            ->notEmptyString('dip_estado');

        $validator
            ->dateTime('dip_fecha_registro')
            ->allowEmptyDateTime('dip_fecha_registro');

        $validator
            ->dateTime('dip_fecha_activacion')
            ->allowEmptyDateTime('dip_fecha_activacion');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->isUnique(['dip_id']), ['errorField' => 'dip_id']);

        return $rules;
    }
}
