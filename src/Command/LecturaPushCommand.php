<?php
namespace App\Command;

use Cake\Command\Command;
use Cake\Console\Arguments;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Filesystem\File;
use Cake\Core\Configure;
use Cake\Http\Client;


class LecturaPushCommand extends Command
{
    protected $modelClass = "GcLocalLecturas";

    protected function buildOptionParser(ConsoleOptionParser $parser): ConsoleOptionParser
    {
        $parser->addArgument('name', [
            'help' => 'What is your name'
        ]);
        return $parser;
    }

    public function execute(Arguments $args, ConsoleIo $io)
    {
        $name = $args->getArgument('name');
        $lecturasObj = array("datapack"=>array());
        $status = "";

        $io->out("Bienvenido {$name}.");
        $io->out("Fecha Actual de ejecucion: ".date(Configure::read('appcfg.dateformat')));


        //Cargando la data a procesar
        $lecturas = $this->GcLocalLecturas->find()
                                        ->where(["GcLocalLecturas.lec_estado"=>"P"])
                                        ->order(["GcLocalLecturas.lec_fecha_registro"=>"desc"])
                                        ->limit(20);        
        $lecturas = $lecturas->all();
        
       
        //Formateando mensaje a enviar
        //TODO: Convertir en funcion utilitaria
        foreach($lecturas as $lecturaKey=>$lecturaVal)
        {
            $lecturaTmp = (object)array("lec_id_remoto"=>0, "lec_dispositivo"=>0, "lec_temperatura"=>0, "lec_amperaje"=>0, "lec_fecha_lectura"=>"");
            $lecturaTmp->lec_id_remoto = $lecturaVal->lec_id;
            $lecturaTmp->lec_dispositivo = $lecturaVal->lec_dispositivo;
            $lecturaTmp->lec_temperatura = intval($lecturaVal->lec_temperatura, 10);
            $lecturaTmp->lec_amperaje = intval($lecturaVal->lec_amperaje, 10);
            $lecturaTmp->lec_fecha_lectura = date(Configure::read('appcfg.dateformat'), strtotime($lecturaVal->lec_fecha_lectura));


            array_push($lecturasObj["datapack"], $lecturaTmp);
        }


        //Log file
        //TODO: Use CakePHP Logging library
        $fp = fopen('/home/administrator/Documents/dev/grainsys/grainsys_web/logs/json_data.json', 'w');
        fwrite($fp, json_encode($lecturasObj));
        fclose($fp);

        
        $io->out("Data a enviar al endpoint");
        $io->out(print_r(json_encode($lecturasObj), true));


        //Post to the endpoint
        try
        {
            $io->out("Conectando con endpoint:");
            $io->out(Configure::read("remotecfg.endpoint_datapack"));            

            $http = new Client();
            $response = $http->post(Configure::read("remotecfg.endpoint_datapack"), 
            json_encode($lecturasObj),
            [
            'type'=>'json'
            ]);

            $io->out("Conexion completada.");
            
            $status = ($response->getStatusCode()=="200")? "0" : "2";
        }
        catch (Exception $ex)
        {
            $io->out("No se pudo realizar conexion con el endpoint.");
            $io->out($ex->getMessage());
            $status = "1";
        }


        //Processing the answer and updating the records on local DB
        if($status=="0")
        {                       
            $io->out("Procesando mensaje recibido.");
            $io->out($response->getStringBody());

            $itemValidation = (object)json_decode($response->getStringBody());
            $itemValidation = $itemValidation->status->itemstatus;
        

            //Processing the answer and saving
            foreach($itemValidation as $itemKey=>$itemData)
            {
                $io->out(print_r($itemData));
                $tablaLecturaLocal = $this->getTableLocator()->get('GcLocalLecturas');
                $lecturaLocal = $tablaLecturaLocal->get($itemData->lec_id_remoto);
                
                $lecturaLocal->lec_estado = $itemData->status;
                $tablaLecturaLocal->save($lecturaLocal);                
            }
        }
        else
        {
            $io->out("Ocurrio un error en el proceso, revise el log para mas detalle. \n Finalizando el proceso.");
        }
        
    }
}
?>