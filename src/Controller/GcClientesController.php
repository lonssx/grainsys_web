<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * GcClientes Controller
 *
 * @property \App\Model\Table\GcClientesTable $GcClientes
 * @method \App\Model\Entity\GcCliente[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GcClientesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $gcClientes = $this->paginate($this->GcClientes);

        $this->set(compact('gcClientes'));
    }

    /**
     * View method
     *
     * @param string|null $id Gc Cliente id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gcCliente = $this->GcClientes->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('gcCliente'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gcCliente = $this->GcClientes->newEmptyEntity();
        if ($this->request->is('post')) {
            $gcCliente = $this->GcClientes->patchEntity($gcCliente, $this->request->getData());
            if ($this->GcClientes->save($gcCliente)) {
                $this->Flash->success(__('The gc cliente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc cliente could not be saved. Please, try again.'));
        }
        $this->set(compact('gcCliente'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Gc Cliente id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gcCliente = $this->GcClientes->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gcCliente = $this->GcClientes->patchEntity($gcCliente, $this->request->getData());
            if ($this->GcClientes->save($gcCliente)) {
                $this->Flash->success(__('The gc cliente has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc cliente could not be saved. Please, try again.'));
        }
        $this->set(compact('gcCliente'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Gc Cliente id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gcCliente = $this->GcClientes->get($id);
        if ($this->GcClientes->delete($gcCliente)) {
            $this->Flash->success(__('The gc cliente has been deleted.'));
        } else {
            $this->Flash->error(__('The gc cliente could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
