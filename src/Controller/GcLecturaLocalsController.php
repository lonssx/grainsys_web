<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
/**
 * GcLecturaLocals Controller
 *
 * @property \App\Model\Table\GcLecturaLocalsTable $GcLecturaLocals
 * @method \App\Model\Entity\GcLecturaLocal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GcLecturaLocalsController extends AppController
{
        /**
     * Initializing method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent("RequestHandler");
    }
    

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $gcLecturaLocals = $this->paginate($this->GcLecturaLocals);

        $this->set(compact('gcLecturaLocals'));
    }

    /**
     * View method
     *
     * @param string|null $id Gc Lectura Local id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gcLecturaLocal = $this->GcLecturaLocals->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('gcLecturaLocal'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gcLecturaLocal = $this->GcLecturaLocals->newEmptyEntity();
        if ($this->request->is('post')) {
            $gcLecturaLocal = $this->GcLecturaLocals->patchEntity($gcLecturaLocal, $this->request->getData());
            if ($this->GcLecturaLocals->save($gcLecturaLocal)) {
                $this->Flash->success(__('The gc lectura local has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc lectura local could not be saved. Please, try again.'));
        }
        $this->set(compact('gcLecturaLocal'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Gc Lectura Local id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gcLecturaLocal = $this->GcLecturaLocals->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gcLecturaLocal = $this->GcLecturaLocals->patchEntity($gcLecturaLocal, $this->request->getData());
            if ($this->GcLecturaLocals->save($gcLecturaLocal)) {
                $this->Flash->success(__('The gc lectura local has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc lectura local could not be saved. Please, try again.'));
        }
        $this->set(compact('gcLecturaLocal'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Gc Lectura Local id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gcLecturaLocal = $this->GcLecturaLocals->get($id);
        if ($this->GcLecturaLocals->delete($gcLecturaLocal)) {
            $this->Flash->success(__('The gc lectura local has been deleted.'));
        } else {
            $this->Flash->error(__('The gc lectura local could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /**
     * 
     * Massive insert method
     * 
     */

    public function massiveadd()
    {
       $viewType = $this->RequestHandler->prefers();
       $status = (object)Array("code"=>0, "message"=>"", "itemstatus"=>Array());
       $dispositivo = $this->request->getData('id_arduidno_box');
       $dataPack = $this->request->getData('lecturas');
       


       try
       {
            //Identifying the reading device
            $dispositivo = TableRegistry::getTableLocator()->get('GcDispositivos')
            ->find()
            ->select(['dip_id'])
            ->where(['GcDispositivos.dip_codigo ='=>$dispositivo])
            ->limit(1);

            $dispositivo = $dispositivo->first();


            //Processing the data received
            if($dispositivo['dip_id']>0)
            {       
                for($dp=0;$dp<count($dataPack);$dp++)
                {
                    /*TODO: Data integrity check*/ 

                    /*Processing the Data*/
                    //print_r($dataPack[$dp]);

                    $gcLecturaLocal = $this->GcLecturaLocals->newEmptyEntity();
                    //$gcLectura = $this->GcLecturas->patchEntity($gcLecturaLocal, $this->request->getData());
                    
                    //Patching
                    $gcLecturaLocal->llc_dispositivo = $dispositivo['dip_id'];
                    $gcLecturaLocal->llc_lectura_1 = (isset($dataPack[$dp]["lectura1"])==false)? "0": $dataPack[$dp]["lectura1"];
                    $gcLecturaLocal->llc_lectura_2 = (isset($dataPack[$dp]["lectura2"])==false)? "0": $dataPack[$dp]["lectura2"];
                    $gcLecturaLocal->llc_lectura_3 = (isset($dataPack[$dp]["lectura3"])==false)? "0": $dataPack[$dp]["lectura3"];
                    $gcLecturaLocal->llc_lectura_4 = (isset($dataPack[$dp]["lectura4"])==false)? "0": $dataPack[$dp]["lectura4"];
                    $gcLecturaLocal->llc_lectura_5 = (isset($dataPack[$dp]["lectura5"])==false)? "0": $dataPack[$dp]["lectura5"];
                    $gcLecturaLocal->llc_lectura_6 = (isset($dataPack[$dp]["lectura6"])==false)? "0": $dataPack[$dp]["lectura6"];
                    $gcLecturaLocal->llc_estado = 'P';
                    $gcLecturaLocal->llc_fecha_lectura = (isset($dataPack[$dp]["fecha"])==false)? "0": $dataPack[$dp]["fecha"];
                    $gcLecturaLocal->llc_fecha_registro = FrozenTime::now(Configure::read('appcfg.timezone'));
                    

                    if ($this->GcLecturaLocals->save($gcLecturaLocal)) 
                    {
                        $status->code = 1;
                        $status->message = "The gc lectura has been saved"; 
                        array_push($status->itemstatus, Array($dp=>"C"));           
                    }
                    else
                    {
                        $status->code = 0;
                        $status->message = "The gc lectura could not be saved. Please, try again";
                        array_push($status->itemstatus, Array($dp=>"E"));
                    }          
                }
            }  
            else
            {
                $status->code = 102;
                $status->message = "The gc lectura could not be saved, device is not valid. Please, try again";
            }                     
        }
        catch(Exception $ex)
        {
            $status->code = 101;
            $status->message = "El proceso de guardado fue interrumpido. Intente nuevamente. [".$ex->getMessage()."]";
        }          


       //Rendering the output
       if($viewType!="json")
       {
           if($status->code==1)
           {
               $this->Flash->success(__($status->message));
               return $this->redirect(['action' => 'index']);
           }
           else
           {
               $this->Flash->error(__($status->message));
           }
       }          


       $this->set(compact('status'));
       $this->viewBuilder()->setOption('serialize', ['status']);
    }    
}
