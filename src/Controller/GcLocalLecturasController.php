<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\Core\Configure;
/**
 * GcLocalLecturas Controller
 *
 * @property \App\Model\Table\GcLocalLecturasTable $GcLocalLecturas
 * @method \App\Model\Entity\GcLocalLectura[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GcLocalLecturasController extends AppController
{
        /**
     * Initializing method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent("RequestHandler");
    }
    

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $gcLocalLecturas = $this->paginate($this->GcLocalLecturas);

        $this->set(compact('gcLocalLecturas'));
    }


    /**
     * View method
     *
     * @param string|null $id Gc Local Lectura id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gcLocalLectura = $this->GcLocalLecturas->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('gcLocalLectura'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gcLocalLectura = $this->GcLocalLecturas->newEmptyEntity();
        if ($this->request->is('post')) {
            $gcLocalLectura = $this->GcLocalLecturas->patchEntity($gcLocalLectura, $this->request->getData());
            if ($this->GcLocalLecturas->save($gcLocalLectura)) {
                $this->Flash->success(__('The gc local lectura has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc local lectura could not be saved. Please, try again.'));
        }
        $this->set(compact('gcLocalLectura'));
    }


    /**
     * Edit method
     *
     * @param string|null $id Gc Local Lectura id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gcLocalLectura = $this->GcLocalLecturas->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gcLocalLectura = $this->GcLocalLecturas->patchEntity($gcLocalLectura, $this->request->getData());
            if ($this->GcLocalLecturas->save($gcLocalLectura)) {
                $this->Flash->success(__('The gc local lectura has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc local lectura could not be saved. Please, try again.'));
        }
        $this->set(compact('gcLocalLectura'));
    }


    /**
     * Delete method
     *
     * @param string|null $id Gc Local Lectura id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gcLocalLectura = $this->GcLocalLecturas->get($id);
        if ($this->GcLocalLecturas->delete($gcLocalLectura)) {
            $this->Flash->success(__('The gc local lectura has been deleted.'));
        } else {
            $this->Flash->error(__('The gc local lectura could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /**
     * 
     * Massive insert method
     * 
     */

    public function massiveadd()
    {
       $viewType = $this->RequestHandler->prefers();
       $status = (object)Array("code"=>0, "message"=>"", "itemstatus"=>Array());
       $dispositivo = $this->request->getData('id_arduidno_box');
       $dataPack = $this->request->getData('lecturas');


       try
       {
            //Identifying the reading device
            $dispositivo = TableRegistry::getTableLocator()->get('GcDispositivos')
            ->find()
            ->select(['dip_id'])
            ->where(['GcDispositivos.dip_codigo ='=>$dispositivo])
            ->limit(1);


            if($dispositivo>0)
            {
                //Processing the data received
                for($dp=0;$dp<count($dataPack);$dp++)
                {
                    /*TODO: Data integrity check*/ 
        
                    /*Processing the Data*/
                    //print_r($dataPack[$dp]);
        
                    $gcLocalLectura = $this->GcLocalLecturas->newEmptyEntity();
                    
                    $gcLocalLectura = $this->GcLocalLecturas->patchEntity($gcLocalLectura, $dataPack[$dp]);
                    $gcLocalLectura->lec_estado = 'P';
                    $gcLocalLectura->lec_fecha_registro = FrozenTime::now(Configure::read('appcfg.timezone'));
        
                    if ($this->GcLocalLecturas->save($gcLocalLectura)) 
                    {
                        $status->code = 1;
                        $status->message = "The gc lectura has been saved"; 
                        array_push($status->itemstatus, Array($dp=>"C"));           
                    }
                    else
                    {
                        $status->code = 0;
                        $status->message = "The gc lectura could not be saved. Please, try again";
                        array_push($status->itemstatus, Array($dp=>"E"));
                    }          
                }   
            }  
            else
            {
                $status->code = 102;
                $status->message = "The gc lectura could not be saved, device is not valid. Please, try again";
            }                     
       }
       catch(Exception $ex)
       {
            $status->code = 101;
            $status->message = "El proceso de guardado fue interrumpido. Intente nuevamente. [".$ex->getMessage()."]";
       }       


       //Rendering the output
       if($viewType!="json")
       {
           if($status->code==1)
           {
               $this->Flash->success(__($status->message));
               return $this->redirect(['action' => 'index']);
           }
           else
           {
               $this->Flash->error(__($status->message));
           }
       }          


       $this->set(compact('status'));
       $this->viewBuilder()->setOption('serialize', ['status']);
    }    
}
