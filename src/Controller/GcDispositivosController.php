<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * GcDispositivos Controller
 *
 * @property \App\Model\Table\GcDispositivosTable $GcDispositivos
 * @method \App\Model\Entity\GcDispositivo[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GcDispositivosController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $gcDispositivos = $this->paginate($this->GcDispositivos);

        $this->set(compact('gcDispositivos'));
    }

    /**
     * View method
     *
     * @param string|null $id Gc Dispositivo id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gcDispositivo = $this->GcDispositivos->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('gcDispositivo'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $gcDispositivo = $this->GcDispositivos->newEmptyEntity();
        if ($this->request->is('post')) {
            $gcDispositivo = $this->GcDispositivos->patchEntity($gcDispositivo, $this->request->getData());
            if ($this->GcDispositivos->save($gcDispositivo)) {
                $this->Flash->success(__('The gc dispositivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc dispositivo could not be saved. Please, try again.'));
        }
        $this->set(compact('gcDispositivo'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Gc Dispositivo id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gcDispositivo = $this->GcDispositivos->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gcDispositivo = $this->GcDispositivos->patchEntity($gcDispositivo, $this->request->getData());
            if ($this->GcDispositivos->save($gcDispositivo)) {
                $this->Flash->success(__('The gc dispositivo has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc dispositivo could not be saved. Please, try again.'));
        }
        $this->set(compact('gcDispositivo'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Gc Dispositivo id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gcDispositivo = $this->GcDispositivos->get($id);
        if ($this->GcDispositivos->delete($gcDispositivo)) {
            $this->Flash->success(__('The gc dispositivo has been deleted.'));
        } else {
            $this->Flash->error(__('The gc dispositivo could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
