<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\I18n\Time;
use Cake\I18n\FrozenTime;
use Cake\Core\Configure;

/**
 * GcLecturas Controller
 *
 * @property \App\Model\Table\GcLecturasTable $GcLecturas
 * @method \App\Model\Entity\GcLectura[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GcLecturasController extends AppController
{
    /**
     * Initializing method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function initialize(): void
    {
        parent::initialize();
        $this->loadComponent("RequestHandler");
    }
    

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        //$this->paginate = ["order"=>["gc_lecturas.lec_id"=>"desc"]];
        $gcLecturas = $this->paginate($this->GcLecturas);
        
        $this->set(compact('gcLecturas'));
    }


    /**
     * View method
     *
     * @param string|null $id Gc Lectura id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $gcLectura = $this->GcLecturas->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('gcLectura'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $viewType = $this->RequestHandler->prefers();
        $status = (object)Array("code"=>0, "message"=>"");

        $gcLectura = $this->GcLecturas->newEmptyEntity();
        if ($this->request->is('post')) {
            $gcLectura = $this->GcLecturas->patchEntity($gcLectura, $this->request->getData());
            $gcLectura->lec_fecha_registro = FrozenTime::now(Configure::read('appcfg.timezone'));

            if ($this->GcLecturas->save($gcLectura)) {
                //$this->Flash->success(__('The gc lectura has been saved.'));
                $status->code = 1;
                $status->message = "The gc lectura has been saved";

                //return $this->redirect(['action' => 'index']);
            }
            else
            {
                $status->code = 0;
                $status->message = "The gc lectura could not be saved. Please, try again";
                //$this->Flash->error(__('The gc lectura could not be saved. Please, try again.'));
            }

            //Rendering the output
            if($viewType!="json")
            {
                if($status->code==1)
                {
                    $this->Flash->success(__($status->message));
                    return $this->redirect(['action' => 'index']);
                }
                else
                {
                    $this->Flash->error(__($status->message));
                }
            }            
        }

        $this->set(compact('gcLectura', 'status'));
        $this->viewBuilder()->setOption('serialize', ['gcLectura', 'status']);
    }


    /**
     * Edit method
     *
     * @param string|null $id Gc Lectura id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $gcLectura = $this->GcLecturas->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $gcLectura = $this->GcLecturas->patchEntity($gcLectura, $this->request->getData());
            if ($this->GcLecturas->save($gcLectura)) {
                $this->Flash->success(__('The gc lectura has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The gc lectura could not be saved. Please, try again.'));
        }
        $this->set(compact('gcLectura'));
    }


    /**
     * Delete method
     *
     * @param string|null $id Gc Lectura id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $gcLectura = $this->GcLecturas->get($id);
        if ($this->GcLecturas->delete($gcLectura)) {
            $this->Flash->success(__('The gc lectura has been deleted.'));
        } else {
            $this->Flash->error(__('The gc lectura could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    /**
     * 
     * Massive insert method
     * 
     */

     public function massiveadd()
     {
        $viewType = $this->RequestHandler->prefers();
        $status = (object)Array("code"=>0, "message"=>"", "itemstatus"=>Array());
        $dispositivo = $this->request->getData('id_arduidno_box');
        $dataPack = $this->request->getData('lecturas');


        try
        {
            //Identifying the reading device
            $dispositivo = TableRegistry::getTableLocator()->get('GcDispositivos')
            ->find()
            ->select(['dip_id'])
            ->where(['GcDispositivos.dip_codigo ='=>$dispositivo])
            ->limit(1);

            $dispositivo = $dispositivo->first();

            debug($dispositivo['dip_id']);


            //Processing the data received
            for($dp=0;$dp<count($dataPack);$dp++)
            {
                /*TODO: Data integrity check*/ 

                /*Processing the Data*/
                //print_r($dataPack[$dp]);

                $gcLectura = $this->GcLecturas->newEntity();
                
                //$gcLectura = $this->GcLecturas->patchEntity($gcLectura, $dataPack[$dp]);
                //Patching
                $gcLectura->llc_dispositivo = $dispositivo['dip_id'];
                $gcLectura->llc_lectura_1 = $dataPack[$dp]["lectura1"];
                $gcLectura->llc_lectura_2 = $dataPack[$dp]["lectura2"];
                $gcLectura->llc_lectura_3 = $dataPack[$dp]["lectura3"];
                $gcLectura->llc_lectura_4 = $dataPack[$dp]["lectura4"];
                $gcLectura->llc_lectura_5 = $dataPack[$dp]["lectura5"];
                $gcLectura->llc_lectura_6 = $dataPack[$dp]["lectura6"];
                $gcLectura->lec_estado = 'P';
                $gcLectura->lec_fecha_lectura = FrozenTime::now(Configure::read('appcfg.timezone'));           //TODO: This must be received later from the device
                $gcLectura->lec_fecha_registro = FrozenTime::now(Configure::read('appcfg.timezone'));

                if ($this->GcLecturas->save($gcLectura) && $dispositivo>0) 
                {
                    $status->code = 0;
                    $status->message = "La lectura fue guardada."; 
                    array_push($status->itemstatus, Array("lec_id_remoto"=>$dataPack[$dp]["lec_id_remoto"], "status"=>"C"));           
                }
                else
                {
                    $status->code = 100;
                    $status->message = "La lectura no pudo ser guardada. Intente nuevamente.";
                    array_push($status->itemstatus, Array("lec_id_remoto"=>$dataPack[$dp]["lec_id_remoto"], "status"=>"E"));
                }          
            }
        }
        catch(Exception $ex)
        {
            $status->code = 101;
            $status->message = "El proceso de guardado fue interrumpido. Intente nuevamente. [".$ex->getMessage()."]";
        }


        //Rendering the output
        if($viewType!="json")
        {
            if($status->code==1)
            {
                $this->Flash->success(__($status->message));
                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->error(__($status->message));
            }
        }          


        $this->set(compact('status'));
        $this->viewBuilder()->setOption('serialize', ['status']);
     }
}
